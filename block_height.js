/*
    Prints the current block height

    Adapted from: https://github.com/aeternity/aepp-sdk-js/blob/develop/docs/index.md

    This requires the following JS line to exist before this file is
    included:

        <script src="https://unpkg.com/@aeternity/aepp-sdk/dist/aepp-sdk.browser-script.js"></script>
*/

async function main()
{
    // the .then() shit is cancer
    // the way I have made sense of this is that "promises" are
    // casts you send to another process, which send back either
    // {ok, Value} or {error, Blah}
    //
    // the 'await' keyword transforms the cast into a call
    let testnet_node = await Ae.Node({url: 'https://testnet.aeternity.io'});
    let my_nodes     = [{name: 'testnet', instance: testnet_node}];
    let ae_instance  = await Ae.Universal({nodes: my_nodes});
    let height       = await ae_instance.height();

    console.log("Current Block Height:", height);
}

main();
