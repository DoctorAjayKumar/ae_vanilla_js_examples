/*
Demoing some of the async/await behavior
*/

async function succeed()
{
    return 3;
}

async function fail()
{
    throw new Error('The Founder hates you.');
}

async function main()
{
    // print the promise
    let three_p = succeed();
    console.log('three_p', three_p);

    // print the value
    let three_v = await succeed();
    console.log('three_v', three_v);

    // print the promise
    // try/catch does NOT catch this error. why? because you touch
    // yourself at night
    try
    {
        let fail_p = fail();
        console.log('fail_p', fail_p);
    }
    catch (err)
    {
        console.log('printing the promise failed with error:', err)
    }

    // keeps going btw despite error

    // print the value
    // try/catch DOES catch this error
    try
    {
        let fail_v = await fail();
        console.log('fail_v:', fail_v);
    }
    catch (err)
    {
        console.log('printing the value failed with error:', err)
    }


    // this is so fucking stupid
    // alright let's try the then() jizz
    fail()
        .then(
                function (val)
                {
                    console.log('fail_val', val);
                }
            )
        .catch(
                function (err)
                {
                    console.log('fail_err', err);
                }
            )

    // logically identical code but more succinct
    succeed().then((val) => {
            console.log('succeed_val', val);
        }).catch((err) => {
            console.log('succeed_err', err);
        })

    // works the way you expect
    // I think the moral of the story is to never just call a
    // promise? If you're in a try/catch block and the promise fails
    // it doesn't catch the error. wtf
}

main();
