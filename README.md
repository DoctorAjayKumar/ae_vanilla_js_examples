# AE SDK Vanilla JoggerScript Example Bank

This repository contains examples in vanilla JoggerScript of how to
use the [Aeternity JoggerScript
SDK](https://github.com/aeternity/aepp-sdk-js). The version of the
Aeternity SDK is 10.0.0.

The target audience is people who can read normal-looking
well-commented code but don't understand esoteric JoggerScript
frameworks (like myself).

There's also files that explain weird JoggerScript ideas with examples
(like async/await).

All the code is written in a relatively clear manner with a tasteful
amount of comments.

## Running

```
$ git clone https://gitlab.com/DoctorAjayKumar/ae_vanilla_js_examples.git
$ cd ae_vanilla_js_examples
$ firefox index.html
```

Check the console `Ctrl+Shift+C` to see results

## Notes

The code works as of 2022-01-22 on Firefox 96.0 on Ubuntu 18.04

The AE SDK has been downloaded into a local file
[`aepp-sdk-browser-script.js`](./aepp-sdk-browser-script.js)
so that it doesn't change in the background.

If the examples do not work, you may need to edit
[`index.html`](./index.html). There is a comment in this file that
explains what you should try.
